<?php

class Hewan extends Fight {
    public $nama,
           $darah = 50,
           $jumlahKaki,
           $keahlian;

    public function atraksi(){
        return "$this->nama" . " sedang " . "$this->keahlian";
    }
}

class Fight{
    public $attackPower,
           $defencePower;

    public function serang(){
       return "{$this->nama}" . " sedang menyerang " . " Harimau " ;
    }

    public function diserang(){
        $str = "{$this->nama}" . " sedang diserang " . " Harimau " 
        . "sehingga darahnya berkurang menjadi " . ($this->darah - 7 )/$this->defencePower;
        return $str;
    }
}


class Elang extends Hewan {
    public $nama = "Elang",
           $darah = 50,
           $jumlahKaki = 2,
           $keahlian = "terbang tinggi",
           $attackPower = 10,
           $defencePower = 5;
}

class Harimau extends Hewan{
    public $nama = "Harimau",
           $darah = 50,
           $jumlahKaki = 4,
           $keahlian = "lari cepat",
           $attackPower = 7,
           $defencePower = 8;
}

$hewan1 = new Elang();
$hewan2 = new Harimau();

echo $hewan1->atraksi();
echo "<br>";
echo $hewan2->atraksi();
echo "<hr>";

echo $hewan1->serang();
echo "<br>";
echo $hewan1->diserang();

?>