<?php

class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $harga,
           $jmlhalaman,
           $wktumain;


     //Contructor Method
     public function __construct($judul= "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0,
     $jmlhalaman = 0, $wktumain = 0) { 
         $this->judul = $judul;
         $this->penulis = $penulis;
         $this->penerbit = $penerbit;
         $this->harga = $harga;
         $this->jmlhalaman = $jmlhalaman;
         $this->wktumain = $wktumain;
     }

    public function getLabel() {
        return "$this->penerbit, $this->penulis";
    }

    public function getLengkap() {
        $str = "{$this->judul} | {$this->getLabel()}  (Rp. {$this->harga})";
        
         return $str;
    }
}

class Komik extends Produk{
    public function getLengkap() {
        $str = "Komik : {$this->judul} | {$this->getLabel()}  (Rp. {$this->harga}) - {$this->jmlhalaman} halaman";
        return $str;
    }
}

class Game extends Produk{
    public function getLengkap() {
        $str = "Game : {$this->judul} | {$this->getLabel()}  (Rp. {$this->harga}) ~ {$this->wktumain} Menit";
        return $str;
    }
}
$produk1 = new Komik("Naruto", "Masashi Kashimoto", "Shonen Jump", 40000, 100, 0);
$produk2 = new Game("Doraemon", "Koroku Masano", "KNIX", 10000, 0, 60);

echo $produk1->getLengkap();
echo "<br>";
echo $produk2->getLengkap();
// var_dump($produk2);

?>