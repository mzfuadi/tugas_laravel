<?php

class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $harga;


     //Contructor Method
     public function __construct($judul= "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0) {
         $this->judul = $judul;
         $this->penulis = $penulis;
         $this->penerbit = $penerbit;
         $this->harga = $harga;
     }

    public function getLabel() {
        return "$this->judul, $this->penulis";
    }
}

$produk1 = new Produk("Naruto", "Masashi Kashimoto", "Shonen Jump", 40000);
$produk2 = new Produk("Doraemon");

echo "Komik :" . $produk1->getLabel();
echo "<br>";
var_dump($produk2);

?>