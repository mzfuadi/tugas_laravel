<?php

class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $harga;


     //Contructor Method
     public function __construct($judul= "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0) { 
         $this->judul = $judul;
         $this->penulis = $penulis;
         $this->penerbit = $penerbit;
         $this->harga = $harga;
     }

    public function getLabel() {
        return "$this->penerbit, $this->penulis";
    }

    public function getLengkap() {
        $str = "{$this->judul} | {$this->getLabel()}  (Rp. {$this->harga})";
        
         return $str;
    }
}

class Komik extends Produk{

    public  $jmlhalaman;

    public function __construct( $judul= "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0,
    $jmlhalaman = 0 ){

        parent::__construct($judul, $penulis , $penerbit , $harga);

        $this->jmlhalaman = $jmlhalaman;
    }

    public function getLengkap() {
        $str = "Komik : " . parent::getLengkap() . " - {$this->jmlhalaman} halaman";
        return $str;
    }
}

class Game extends Produk{

    public $wktumain;

    public function __construct( $judul= "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0,
    $wktumain = 0 ){

        parent::__construct($judul, $penulis , $penerbit, $harga );

        $this->wktumain = $wktumain;
    }

    public function getLengkap() {
        $str = "Game : " . parent::getLengkap() . " ~ {$this->wktumain} Menit";
        return $str;
    }
}
$produk1 = new Komik("Naruto", "Masashi Kashimoto", "Shonen Jump", 40000, 100);
$produk2 = new Game("Doraemon", "Koroku Masano", "KNIX", 10000, 60);

echo $produk1->getLengkap();
echo "<br>";
echo $produk2->getLengkap();
// var_dump($produk2);
echo "<hr>";

?>